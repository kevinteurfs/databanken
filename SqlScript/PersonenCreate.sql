USE modernways;
CREATE TABLE Personen (
    Id int AUTO_INCREMENT,
    Voornaam nvarchar(255) NOT NULL,
    Familienaam nvarchar(255) NOT NULL,
    Leeftijd int,
    CONSTRAINT pk_Personen_Id PRIMARY KEY (Id)
);