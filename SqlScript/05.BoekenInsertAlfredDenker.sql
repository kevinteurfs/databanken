use Modernways;
insert into Boeken (Voornaam, 
                    Familienaam, 
                    Titel, 
                    Uitgeverij, 
                    Verschijningsjaar, 
                    Insertedby)
Values (
    'Alfred',
    'Denker',
    'Onderweg in Zijn en Tijd',
    'Danon',
    '2017',
    'Kevin Teurfs');