-- KT
-- 2 mei 2018
--

USE ModernWays;

DROP TABLE IF EXISTS `VandeBorre`;

-- de naam van de tabel in Pascalnotatie
CREATE TABLE VandeBorre(
    Naam NVARCHAR(100),
    Adres NVARCHAR(100),
   	Postcode CHAR(4),
    Stad NVARCHAR(100),
    Telefoon NVARCHAR(100),
    Id int auto_increment not null,
    constraint pk_boeken_Id primary key(id)    
);