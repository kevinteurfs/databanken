--opdracht 1 --
use modernways;
select  Voornaam,
        Familienaam,
        Titel
  from Boeken
  where Verschijningsjaar between '1998' and '2001';
 -- opdracht 2 --
use modernways;
select  Voornaam,
        Familienaam,
        Titel
  from Boeken
  where Verschijningsjaar between '1900' and '2011' 
	and  Familienaam like 'B%'
order by familienaam asc;
-- opdracht 3 --
use modernways;
select  Voornaam,
        Familienaam,
        Titel
  from Boeken
  where Verschijningsjaar between '1900' and '2011' 
	and  Familienaam like 'B%'
order by familienaam asc;