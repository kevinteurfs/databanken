--taak 2
use modernways;
select Voornaam, Familienaam, Titel from Boeken
where Voornaam = 'J.K.' or Voornaam = 'Evert w.';

--taak 3
use modernways;
select Voornaam, Familienaam, Titel from Boeken
 where not Voornaam = 'J.K.';

--taak 4 tot 6
use modernways;
insert into Boeken (
   Voornaam,
   Familienaam,
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsjaar,
   Herdruk,
   Commentaar,
   Categorie,
   InsertedBy
)
values 
('Hugo', 'Claus', 'De verwondering', 'Antwerpen', 'Manteau', '1970', '', '', '', 'KT'),
('Hugo', 'Raes', 'Jagen en gejaagd worden', 'Antwerpen', 'De Bezige Bij', '1954', '', '', '', 'KT'),
('Jean-Paul', 'Sarthe', 'Het zijn en het niets','Parijs' , 'Gallimard', '1943', '', '','', 'KT');

--taak 7
use modernways;
select Voornaam, Familienaam, Titel from Boeken
	where Voornaam = 'Hugo' or Voornaam = 'Jean-paul';

--taak 8
use modernways;
select Voornaam, Familienaam, Titel from Boeken
	where (Voornaam = 'Hugo' or Voornaam = 'Jean-paul') 
	and Verschijningsjaar = '1970';

--taak 9
use modernways;
update Boeken
	set Familienaam = 'Sartre'
	where Titel = 'Het zijn en het niets';
select * from Boeken;

--taak 10
use modernways;
update Boeken
	set Categorie = 'Literatuur'
	where Voornaam = 'Hugo';
select * from Boeken;

--taak 11
use modernways;
update Boeken
	set Categorie = 'filosofie'
	where (Familienaam = 'Sartre' and Voornaam = 'Jean-paul' and Titel = 'Het zijn en het niets');
select * from Boeken;

--taak 12
use modernways;
select Voornaam, Familienaam, Titel from Boeken
 where (Familienaam = 'Sartre' and Categorie = 'Filosofie') 
or (Voornaam = 'Hugo' and Familienaam = 'Claus');