-- JI
-- 21 februari 2018
--

USE ModernWays;

DROP TABLE IF EXISTS `Postcodes`;

-- de naam van de tabel in Pascalnotatie
CREATE TABLE Postcodes(
    Code CHAR(4),
    Plaats NVARCHAR(255),
    Provincie NVARCHAR(255),
    Localite NVARCHAR(255),
    Provence NVARCHAR(255)
);

