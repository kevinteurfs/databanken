-- KT
-- 02 Mei 2018
-- Bestandsnaam: PostcodesAlterPrimaryKeyAdd.sql
-- eerst een kolom Id toevoegen en dan dan de constraint toevoegen
use ModernWays;
ALTER TABLE Postcodes ADD Id INT auto_increment, ADD CONSTRAINT pk_Person_Id PRIMARY KEY (Id);