-- KT
-- 18 April 2018
-- kollom id toevoegen en primary key van maken
--
use modernways;
alter table Boeken add Id INT AUTO_INCREMENT,
    add CONSTRAINT pk_Boeken_Id PRIMARY KEY (Id);