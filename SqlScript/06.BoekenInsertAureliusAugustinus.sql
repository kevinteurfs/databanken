use Modernways;
insert into Boeken (
   Voornaam,
   Familienaam,
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsjaar,
   Herdruk,
   Commentaar,
   Categorie,
   InsertedBy
)
values
('Aurelius', 'Augustinus', 'De stad van God', 'Baarn', 'Uitgeverij Baarn', '1983', '1992', '', '', ''),
('Diderik', 'Batens', 'Logicaboek', '', 'Garant', '1999', '', '', '', '')